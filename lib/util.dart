get isDebug {
  try {
    assert(false);
  } on AssertionError {
    return true;
  }
  return false;
}