import 'dart:async' as asc;
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:one_pointed/util.dart';
import 'package:screen/screen.dart';

class Timer extends StatefulWidget {
  final Duration duration;

  Timer(this.duration);

  @override
  State<StatefulWidget> createState() => new TimerState();
}

class TimerState extends State<Timer> {
  asc.Timer _timer;
  DateTime _startTime;
  final GlobalKey<AnimatedCircularChartState> _chartKey =
      new GlobalKey<AnimatedCircularChartState>();

  _getChartData(num completion) {
    return [
      new CircularStackEntry([
        new CircularSegmentEntry(completion, Theme.of(context).primaryColor)
      ])
    ];
  }

  _updateTime() {
    var timePassed = new DateTime.now().difference(_startTime).inMilliseconds;
    var completion = max(0.5, timePassed * 100 / widget.duration.inMilliseconds);
    _chartKey.currentState.updateData(_getChartData(completion));

    if (completion >= 100.0) {
      _timer.cancel();
    }
  }

  _handleTap() {
    if (!_timer.isActive) {
      if (!isDebug) {
        exit(0);
      } else {
        print('App exit');
      }
    }
  }

  @override
  initState() {
    super.initState();
    Screen.keepOn(true);
    _startTime = new DateTime.now();
    _timer = new asc.Timer.periodic(new Duration(milliseconds: 500), (_) {
      setState(_updateTime);
    });
  }

  Widget build(context) {
    var stackItems = <Widget>[
      new AnimatedCircularChart(
        key: _chartKey,
        size: const Size(200.0, 200.0),
        initialChartData: _getChartData(0.0),
        chartType: CircularChartType.Radial,
        percentageValues: true,
      ),
    ];

    var centerItems = <Widget>[
      new Stack(alignment: new Alignment(0.0, 0.0), children: stackItems),
    ];

    if (!_timer.isActive) {
      stackItems.add(new Container(
        width: 135.0,
        height: 135.0,
        decoration: new ShapeDecoration(
          shape: new CircleBorder(),
          color: Theme.of(context).primaryColor,
        ),
      ));

      centerItems.add(new Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: new Text('Meditation complete :)\nTap anywhere to exit.',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.caption),
      ));
    }

    return new Scaffold(
        body: new GestureDetector(
            onTap: _handleTap,
            // Use an extra container to expand the gestureDetector to the whole screen
            // Somehow we also need to set a color for gesture detection to work
            child: new Container(
              color: Colors.transparent,
                child: new Center(
              child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: centerItems),
            ))));
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
    Screen.keepOn(false);
  }
}
