import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:one_pointed/home.dart';

get theme => new ThemeData(
  primarySwatch: Colors.green,
);

void main() async {
  runApp(new MyApp());
  SystemChrome.setEnabledSystemUIOverlays([]);
  HapticFeedback.vibrate();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'one-pointed',
      home: new Home(),
      theme: theme,
    );
  }
}

