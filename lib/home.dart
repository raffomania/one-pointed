import 'package:flutter/material.dart';
import 'package:one_pointed/timer.dart';
import 'package:one_pointed/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new HomeState();
}

class HomeState extends State<Home> {
  var _fieldKey = new GlobalKey<FormFieldState>(debugLabel: 'timeField');
  final _inputController = new TextEditingController();

  initState() {
    super.initState();
    _loadMeditationTime();
  }

  _loadMeditationTime() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      var savedTime = prefs.getInt('meditation_minutes');
      if (savedTime != null) {
        _inputController.text = savedTime.toString();
        var offset = savedTime.toString().length;
        _inputController.selection = new TextSelection(baseOffset: 0, extentOffset: offset);
      }
    });
  }

  _onSubmit([_]) async {
    if (_fieldKey.currentState.validate()) {
      var minutes = int.parse(_fieldKey.currentState.value);

      final prefs = await SharedPreferences.getInstance();
      prefs.setInt('meditation_minutes', minutes);

      var duration = new Duration(minutes: minutes);

      if (isDebug) {
        duration = new Duration(seconds: minutes);
      }

      Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
        return new Timer(duration);
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        padding: const EdgeInsets.all(15.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: new Text('How long do you want to meditate?')),
            new TextFormField(
              validator: (input) {
                try {
                  int.parse(input);
                  return null;
                } on Exception {
                  return 'Please enter a whole number';
                }
              },
              controller: _inputController,
              key: _fieldKey,
              decoration: new InputDecoration(labelText: 'time in minutes'),
              autocorrect: false,
              keyboardType: TextInputType.number,
              onFieldSubmitted: _onSubmit,
            ),
            new Container(
              padding: const EdgeInsets.only(top: 25.0),
              alignment: Alignment.centerRight,
              child: new RaisedButton.icon(
                color: Theme.of(context).primaryColor,
                label: const Text('GO'),
                icon: const Icon(Icons.play_arrow),
                onPressed: _onSubmit,
                textColor: Theme.of(context).primaryTextTheme.button.color,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
